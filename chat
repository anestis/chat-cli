#!/usr/bin/perl

use warnings;
use strict;

use LWP::UserAgent;
use HTTP::Request::Common;
use Digest::SHA qw( sha256_hex );
use Digest::HMAC;
use JSON::XS;
use MIME::Base64;
use Time::HiRes qw( usleep );       # more flexible that sleep
use Config::Tiny;
use Data::Dumper;
use Time::Piece;
use Text::Wrap;
use AWS::Sig4;

# Set to 1 to enable debug model
my $debug = 0;

# Define content output width
my $line_width = 80;

# Your AWS access key and secret key
my $access_key = $ENV{'AWS_ACCESS_KEY_ID'};
my $secret_key = $ENV{'AWS_SECRET_ACCESS_KEY'};

my $provider = 'aws'; # alternatively 'openai'

# Load config file and define config variables
my $config = Config::Tiny->read('config.ini');

my $use_tts         = $config->{'GENERAL'}->{'use_tts'};       # use TTS API?
my $IBM_API_KEY     = $config->{'IBM'}->{'apikey'};
my $ibm_url         = $config->{'IBM'}->{'url'};
my $voice           = $config->{'IBM'}->{'voice'};             # en-US_AllisonV3Voice
my $OPENAI_API_KEY  = $ENV{OPENAI_API_KEY} || $config->{'OPENAI'}->{'apikey'};
my $openai_url      = $config->{'OPENAI'}->{'url'};            # gpt-3.5-turbo
my $model           = $provider eq 'openai' ? $config->{'OPENAI'}->{'model'} : $config->{'BEDROCK'}->{'model'};
my $role            = $config->{'OPENAI'}->{'role'};           # assistant
my $output_format   = $config->{'OPENAI'}->{'output_format'};  # audio/wav
my $bedrock_api     = $config->{'BEDROCK'}->{'api'};

# Autoflush stdout    
$| = 1;

# Concatenate input words into one single argument
my $prompt = join(" ", @ARGV);

# Function to implement the call to OPENAI API
sub openai_api {
    my $ua = LWP::UserAgent->new;

    # Define authorization header
    my $auth_header = 'Bearer ' . $OPENAI_API_KEY;

    # Headers for the post request
    my $headers = [
        Content_Type      => 'application/json',
        Authorization     => $auth_header
    ];

    # Data to send with the post request
    my $data = {
        'model'     => $model,
        'messages'  => [
            {
                'role'      => $role,
                'content'   => $prompt  # user input
            }
        ]
    };

    # Implement post request
    my $response = $ua->post(
        $openai_url,
        @$headers,
        Content => encode_json($data)
    );

    if ($response->is_success) {
        return decode_json($response->content)->{choices}->[0]->{message}->{content}; 
    } else {
        die "Error: " . $response->status_line;
    }

}

sub format_aws_timestamp {
    my $timestamp = gmtime();  # Get the current timestamp
    $timestamp = $timestamp->strftime('%Y%m%dT%H%M%SZ');  # Format it in ISO-8601 basic format
    return $timestamp;
}

sub aws_api {
    my $service_name = 'bedrock';
    my $region = 'us-east-1';
    my $date = format_aws_timestamp();

    my $signer = AWS::Signature4->new(
        -access_key => $access_key,
        -secret_key => $secret_key
    );

    my $ua     = LWP::UserAgent->new();

    my $data = {
        "presencePenalty" => {
            "scale" => 0
        },
        prompt => $prompt,
        "countPenalty" => {
            "scale" => 0
        },
        "maxTokens" => 500,
        "stopSequences" => [],
        "temperature" => 0.7,
        "frequencyPenalty" => {
            "scale" => 0
        },
        "topP" => 1
    };

    my $headers = [
        'accept' => '*/*',
        'content-type' => 'application/json',
    ];

    my $api_url = 'https://bedrock-runtime.us-east-1.amazonaws.com/model/' . $model . '/invoke';

    # Define the HTTP request
    my $request = HTTP::Request->new(
        'POST',
        $api_url,
        $headers,
        JSON::XS->new->utf8->encode($data)
    );

    $signer->sign($request);

    if ($debug) {
        $ua->add_handler("request_send",  sub { shift->dump; return });
        $ua->add_handler("response_done", sub { shift->dump; return });
    }

    # Send the HTTP request
    my $response = $ua->request($request);

    if (! $debug) {
        # Check the response
        if ($response->is_success) {
            return decode_json($response->{_content})->{completions}->[0]->{data}->{text};
        } else {
            #print Dumper(\$response);
            die "Error: " . $response->status_line;
        }
    }

}

# Subroutine to implement the IBM Text-to_Speech API call
sub tts_api {
    my $text = shift; 

    my $ua_tts = LWP::UserAgent->new;

    my $str = {
        'text' => $text
    };

    my $auth_header = 'Basic ' . encode_base64("apikey:" . $IBM_API_KEY);

    my $headers = [
        Content_Type      => 'application/json',
        Accept            => $output_format,
        Authorization     => $auth_header
    ];

    my $response = $ua_tts->post(
        $ibm_url . '/v1/synthesize?voice=' . $voice,      # voice is a URL parameter
        @$headers,
        Content => encode_json($str)
    );

    if ($response->is_success) {
        open(my $fh, '>', 'output.wav') or die "Failed to open audio player: $!";
        print $fh $response->content;
        close($fh);
    } else {
        print "Error: " . $response->status_line;
    }
}

my $output = $provider eq 'aws' ? &aws_api() : &openai_api();

# Use TTS API only if flag is set to 1
if ($use_tts) {
    &tts_api($output);

    if ( -s 'output.wav' ) {
        system('aplay output.wav 2> /dev/null &');
    } else {
        print("\n\nNo audio output\n\n");
    }
}

print "\n";
print "=== MACHINE ====================================================================";
print "\n";

foreach my $char (split "", wrap("", "", $output)) {
    print $char;
    usleep 40000;
}

print "\n\n";
print "================================================================================";
print "\n\n";

# Create a simple perldoc documentation
=head1 NAME

    Chat - Perl script to interact via text and audio with OpenAI's ChatGPT

=head1 SYNOPSIS

    chat <prompt>

    Returns text to STDOUT and (optionally) audio in WAV format

=head1 DESCRIPTION

    Chat is a perl script running on Linux terminal applications that let you interact with OpenAI's ChatGPT language model. Apart from text returned from OpenAI API, it also returns audio via the IBM's Text-To-Speech service API. In order for the script to run, you need to have the OpenAI's API key stored as an environment variable on your machine. Also, the script should be accompannied with a config file named `config.ini` in the same directory. This config file will include the IBM's API key for the Text-To-Speech service and also the specific IBM URL to access the service.


=cut
