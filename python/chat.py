#!/usr/bin/python3

import boto3
import json
import time

prompt = input("?> ")
model_id = "ai21.j2-ultra-v1"
content_type = "application/json"
accept = "*/*"
body = "{\"prompt\":\"" + prompt + "\",\"maxTokens\":200,\"temperature\":0.7,\"topP\":1,\"stopSequences\":[],\"countPenalty\":{\"scale\":0},\"presencePenalty\":{\"scale\":0},\"frequencyPenalty\":{\"scale\":0}}"

bedrock_runtime = boto3.client(
    service_name = "bedrock-runtime",
    region_name = "us-east-1"
)

kwargs = {
    "modelId": model_id,
    "contentType": content_type,
    "accept": accept,
    "body": body
}

response = bedrock_runtime.invoke_model(**kwargs)

response_body = json.loads(response.get('body').read())

completion = response_body.get('completions')[0].get('data').get('text')

for char in completion:
    print(char, end="", flush=True)
    time.sleep(0.05)

print()
