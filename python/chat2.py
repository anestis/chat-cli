#!/usr/bin/python3

import boto3
import json
import time
import tkinter as tk


bedrock_runtime = boto3.client(
    service_name = "bedrock-runtime",
    region_name = "us-east-1"
)

# Function to handle the API request
def send_request():
    user_input = entry.get()
    response = get_api_response(user_input)
    output.config(state="normal")  # Enable the text widget for editing
    output.delete(1.0, tk.END)  # Clear the existing content
    for char in response:
        output.insert(tk.END, char)  # Insert the API response
        time.sleep(0.05)
    output.config(state="disabled")  # Disable the text widget for editing

def get_api_response(input_text):
    model_id = "ai21.j2-ultra-v1"
    content_type = "application/json"
    accept = "*/*"
    body = "{\"prompt\":\"" + input_text + "\",\"maxTokens\":200,\"temperature\":0.7,\"topP\":1,\"stopSequences\":[],\"countPenalty\":{\"scale\":0},\"presencePenalty\":{\"scale\":0},\"frequencyPenalty\":{\"scale\":0}}"

    kwargs = {
        "modelId": model_id,
        "contentType": content_type,
        "accept": accept,
        "body": body
    }

    response = bedrock_runtime.invoke_model(**kwargs)

    response_body = json.loads(response.get('body').read())

    completion = response_body.get('completions')[0].get('data').get('text')

    return completion


# Create the main window
app = tk.Tk()
app.title("API Request App")

# Create a custom font
custom_font = ("Helvetica", 12)  # Change "Helvetica" to the desired font family and adjust the size

# Create an input field
entry = tk.Entry(app, font=custom_font)
entry.pack(pady=10)

# Create a button to send the request
send_button = tk.Button(app, text="Send Request", command=send_request, font=custom_font)
send_button.pack()

# Create a text widget for displaying the API response
output = tk.Text(app, wrap=tk.WORD, height=10, width=40, font=custom_font)
output.pack()

app.mainloop()
