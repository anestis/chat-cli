# Simple ChatGPT interaction in the terminal

## Description

A perl script to interact with OpenAI's ChatGPT in the terminal. Optionally, the user can enable Text-to-Speech, in order to receive the response also with voice via the IBM's Text-To-Speech API.

## Getting Started

### Dependencies

* Linux/Unix OS
* Perl
    * LWP::UserAgent
    * Config::Tiny
* aplay

### Installing

* Make sure that dependencies are satisfied
* Clone the repo or download the files chat and config.ini into a local directory
* Modify the config.ini file by replacing the following keys:
    * IBM.apikey
    * IBM.url
    * OPENAI.apikey  

### Executing program

``` chat _prompt_ ```

## Authors

Anestis Kotidis [site](https://anestiskotidis.com)
